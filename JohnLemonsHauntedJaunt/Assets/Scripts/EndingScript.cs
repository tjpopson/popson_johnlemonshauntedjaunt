﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndingScript : MonoBehaviour
{
    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    float m_Timer;
    private float spookMeter = 10f;

    bool m_IsPlayerAtExit;
    bool m_IsPlayerCaught;

    public GameObject player;
    public CanvasGroup exitBackgroundImageCanvasGroup;
    public CanvasGroup caughtBackgroundImageCanvasGroup;
    public Text spookText;

    public AudioSource exitAudio;
    public AudioSource caughtAudio;
    bool m_HasAudioPlayed;

    public float getSpook() {

        return spookMeter;
    
    }

    public void setSpook(float spook) {

        if (spook >= 0) {
            spookMeter += spook;
        }
    
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            m_IsPlayerAtExit = true;
        }
    }

    public void CaughtPlayer()
    {
        
            m_IsPlayerCaught = true;

    }
    
    void Update()
    {
        if (m_IsPlayerAtExit)
        {
            EndLevel(exitBackgroundImageCanvasGroup, false, exitAudio);
        }
        else if (m_IsPlayerCaught || spookMeter <= 0)
        {
            EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);
        }

        if (spookMeter >= 0)
        {
            spookMeter -= Time.deltaTime;
        }

        spookText.text = "Sanity: " + Mathf.Floor(spookMeter).ToString();
    }

    void EndLevel(CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource)
    {
        m_Timer += Time.deltaTime;
        spookText.enabled = false;

        imageCanvasGroup.alpha = m_Timer / fadeDuration;

        if (!m_HasAudioPlayed)
        {
            audioSource.Play();
            m_HasAudioPlayed = true;
        }

        if (m_Timer > fadeDuration + displayImageDuration)
        {
            if (doRestart)
            {
                SceneManager.LoadScene(0);
            }
            else
            {
                Application.Quit();
            }
        }
    }
}
