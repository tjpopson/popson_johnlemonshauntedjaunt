﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupScript : MonoBehaviour
{
    public EndingScript script;
    public GameObject player;
    public GameObject pickup;

    public float time = 3;

    void Start()
    {
        pickup.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player) {

            script.setSpook(time);
            pickup.SetActive(false);
        
        }
    }
}
