﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    Animator m_Animator;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;

    public float turnSpeed = 20f;

    // Start is called before the first frame update
    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;

        m_Animator.SetBool("IsWalking", isWalking);
        m_Animator.SetBool("IsSprinting", Input.GetKey("left shift"));

        if (isWalking)
        {
            if (m_AudioSource != null && !m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            try
            {
                if (m_AudioSource != null) {
                    m_AudioSource.Stop();
                }
            }
            catch (Exception e) {
                Debug.Log("Audio stop error");
                Debug.Log(e.Message);
            }
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);

    }

    void OnAnimatorMove()
    {
        try
        {
            if (m_Rigidbody != null) {
                m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
                m_Rigidbody.MoveRotation(m_Rotation);
            }
        }
        catch (Exception e) {
                Debug.Log("RigidBody stop error");
                Debug.Log(e.Message);
        }
    }
}