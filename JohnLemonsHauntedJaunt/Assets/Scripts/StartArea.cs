﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartArea : MonoBehaviour
{

    public Text info;
    public GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        info.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player) {

            info.enabled = false;
        
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {

            info.enabled = true;

        }
    }
}
